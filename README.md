## Introduction

project consists of 5 main Controllers
* ReportController: for making needed reports
* WalletController: for wallet information and functionalities
* VoucherController: for voucher admin controls
* VoucherConsumeController: for consuming voucher functionalities
* UserController: for modify and create users as an admin

project consists of 2 main Services
* WalletService: to handle Wallet functionalities like creating wallet for user and do a transaction
* VoucherService: to handle voucher functionalities


##### WalletService
Due to the requirement of avoiding data consistency problems for handling a transaction, Database transaction 
used for transaction functionality, hence to avoid race conditions database update locks used. 

#### VoucherService
Due to sequential state of Voucher usage functionality and necessity of raising exception after failure in each step
Chain of responsibility Design pattern used for the Voucher consumption functionality, Meanwhile Database transactions 
and database update lock in optimistic manner is used for avoid data consistency problems and race conditions.
race conditions could be handled by redis atomic locks too, but database locks used due to lack of time


### Setup
1. The project is configured to use an SQLite database for ease of use.
2. for ease of use .env file is added to git
3. please add database.sqlite file inside database folder

```
composer install
```

### make database tables 

```
php artisan migrate:refesh 
```


## Endpoints

Postman file arvan-chalenge.postman_collection.json is provided for api documentation in project root.

**POST /api/consumeVoucher** - voucher consumption

Available x-www-form-urlencoded parameters:\
`mobile` - mobile number \
`code` - voucher code 


---

**GET /api/reports/voucherUsers** - get successful usages of the voucher code by users


Required parameters:\
`code` - voucher code \
`perPage` - per Page  records \
`page` - page number


---

**GET api/wallet/info** - get user's wallet info


Required parameters:\
`mobile` - mobile number 


Sample response (HTTP 200)
```
{
    "data": [
        {
            "id": 3,
            "amount": 5,
            "description": "Voucher code",
            "wallet_id": 1,
            "created_at": "2023-10-28T06:44:06.000000Z",
            "updated_at": "2023-10-28T06:44:06.000000Z"
        },
        {
            "id": 4,
            "amount": 4343,
            "description": "Voucher code",
            "wallet_id": 1,
            "created_at": "2023-10-28T07:33:03.000000Z",
            "updated_at": "2023-10-28T07:33:03.000000Z"
        }
    ],
    "links": {
        "first": "http://127.0.0.1:8000/api/wallet/info?page=1",
        "last": "http://127.0.0.1:8000/api/wallet/info?page=2",
        "prev": "http://127.0.0.1:8000/api/wallet/info?page=1",
        "next": null
    },
    "meta": {
        "current_page": 2,
        "from": 3,
        "last_page": 2,
        "links": [
            {
                "url": "http://127.0.0.1:8000/api/wallet/info?page=1",
                "label": "&laquo; Previous",
                "active": false
            },
            {
                "url": "http://127.0.0.1:8000/api/wallet/info?page=1",
                "label": "1",
                "active": false
            },
            {
                "url": "http://127.0.0.1:8000/api/wallet/info?page=2",
                "label": "2",
                "active": true
            },
            {
                "url": null,
                "label": "Next &raquo;",
                "active": false
            }
        ],
        "path": "http://127.0.0.1:8000/api/wallet/info",
        "per_page": 2,
        "to": 4,
        "total": 4
    },
    "walletInfo": {
        "id": 1,
        "user_id": 1,
        "balance": 4358,
        "created_at": "2023-10-28T06:38:26.000000Z",
        "updated_at": "2023-10-28T07:33:03.000000Z"
    }
}
```


---

some other apis is provided to handle vouchers , users , etc in arvan-chalenge.postman_collection.json in root folder

## Docker
for ease of use docker-compose file added and the database is still sqlite and its bind mounted on the server's volume
you can uncomment the mysql section if you wish to use mysql,but it has extra steps for adding user accesses

```
docker compose build && docker compose up  -d
```
