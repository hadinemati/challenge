FROM php:8.2-fpm-alpine

ARG WORKDIR=/var/www/html

WORKDIR $WORKDIR
# Install system dependencies
RUN apk update && apk add --no-cache \
    git \
    shadow \
    curl \
    zip \
    unzip \
    nano \
    bash \
    autoconf \
    build-base\
    supervisor

# Install PHP extensions
#in case of gd uncomment below
#docker-php-ext-configure gd --with-freetype --with-jpeg
RUN docker-php-ext-install  pcntl bcmath

# Install Redis and enable it
RUN pecl install redis  && docker-php-ext-enable redis


# Install the PHP pdo_mysql extention
#RUN docker-php-ext-install pdo_mysql

# Install the PHP pdo_pgsql extention
#RUN docker-php-ext-install pdo_pgsql


# Install Composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

RUN rm -Rf /var/www/* && \
mkdir -p /var/www/html


COPY .env.production $WORKDIR/.env

COPY composer.json composer.lock  $WORKDIR/

RUN composer install --no-scripts --no-autoloader


ADD supervisor.conf /etc/supervisor/conf.d/laravel-worker.conf
ADD supervisor.conf /etc/supervisord.conf

COPY . $WORKDIR
RUN usermod -u 1000 www-data

# Assign permissions of the working directory to the www-data user
RUN chown -R www-data:www-data \
 /var/www/html/storage \
 /var/www/html/bootstrap/cache


RUN composer dump-autoload
RUN php artisan optimize
RUN php artisan migrate --force


EXPOSE 9000
