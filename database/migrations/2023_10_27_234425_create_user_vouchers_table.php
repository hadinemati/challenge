<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up()
    {
        Schema::create('user_voucher', function (Blueprint $table) {
            $table->id();

            $table->integer('user_id');
            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->cascadeOnDelete();

            $table->integer('voucher_id');
            $table->foreign('voucher_id')
                ->references('id')
                ->on('vouchers')
                ->cascadeOnDelete();

            $table->unique(['user_id','voucher_id']);

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('user_voucher');
    }
};
