<?php

namespace App\Exceptions;

use App\Enums\ErrorCodes;
use Exception;

abstract class CustomExceptionAbstract extends Exception
{
    use CustomExceptionTrait;

    public function __construct(ErrorCodes $errorCodes)
    {
        $this->httpCode = $errorCodes->getHttpCode();
        parent::__construct($errorCodes->getMessage(), $errorCodes->getCode(), $previous = null);
    }

}
