<?php

namespace App\Exceptions;

use App\Enums\ErrorCodes;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * The list of the inputs that are never flashed to the session on validation exceptions.
     *
     * @var array<int, string>
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

//    protected $dontReport = [
//        //will not log these exceptions
//    ];
    /**
     * Register the exception handling callbacks for the application.
     */
    public function register(): void
    {
        $this->reportable(function (Throwable $e) {
            //
        });
    }

    public function render($request, Throwable $e)
    {
        return match (true) {
            $e instanceof CustomExceptionAbstract =>
            new JsonResponse(
                [
                    "message" => $e->getMessage(),
                    "code" => $e->getCode()
                ],
                $e->getHttpCode()
            ),

            $e instanceof ModelNotFoundException || $e instanceof NotFoundHttpException =>
            new JsonResponse(
                [
                    "message" => ErrorCodes::NOT_FOUND->getMessage(),
                    "code" => ErrorCodes::NOT_FOUND->getCode()
                ],
                ErrorCodes::NOT_FOUND->getHttpCode()
            ),

            default => parent::render($request, $e)
        };
    }
}
