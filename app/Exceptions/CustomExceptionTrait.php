<?php

namespace App\Exceptions;

trait CustomExceptionTrait
{
    public int $httpCode;

    public function getHttpCode(): int
    {
        return $this->httpCode;
    }
}
