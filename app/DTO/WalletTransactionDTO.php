<?php

namespace App\DTO;

use App\Models\Wallet;

class WalletTransactionDTO
{
    public function __construct(
        public readonly Wallet $wallet,
        public readonly float  $transferAmount,
        public readonly string $description
    )
    {
    }

    public function getWallet(): Wallet
    {
        return $this->wallet;
    }

    public function getTransactionAmount(): float
    {
        return $this->transferAmount;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function __get(string $name)
    {
        return $this->{$name};
    }
}
