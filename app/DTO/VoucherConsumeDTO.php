<?php

namespace App\DTO;

use App\Models\Voucher;
use App\Models\Wallet;

class VoucherConsumeDTO
{

    public function __construct(public readonly Voucher $voucher, public readonly Wallet $wallet)
    {

    }

    public function getWallet(): Wallet
    {
        return $this->wallet;
    }

    public function getVoucher(): Voucher
    {
        return $this->voucher;
    }

    public function __get(string $name)
    {
        return $this->{$name};
    }
}
