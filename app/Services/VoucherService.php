<?php

namespace App\Services;

use App\DTO\VoucherConsumeDTO;
use App\Services\VoucherServicePipelines\CheckVoucher;
use App\Services\VoucherServicePipelines\ConsumeVoucher;
use DB;
use Exception;
use Illuminate\Pipeline\Pipeline;
use Log;

class VoucherService
{
    /**
     * @param VoucherConsumeDTO $consumeDTO
     * @return mixed
     * @throws Exception
     */
    public function consumeVoucher(VoucherConsumeDTO $consumeDTO): mixed
    {
        // consuming voucher will be a sequential work in which in every step
        // if it fails it should not continue the process so Chain Of Responsibility Design
        // Pattern picked for this functionality
        $pipeline = [
            CheckVoucher::class,
            ConsumeVoucher::class
        ];

        $pipelineHandler = resolve(Pipeline::class);

        DB::beginTransaction();

        try {
            $res = $pipelineHandler->send($consumeDTO)
                ->through($pipeline)
                ->thenReturn();

            DB::commit();

            return true;
        } catch (Exception $exception) {
            DB::rollBack();
            Log::error($exception->getMessage());
            //add to external logging tool
            throw $exception;
        }
    }
}
