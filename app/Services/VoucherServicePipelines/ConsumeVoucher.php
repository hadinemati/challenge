<?php

namespace App\Services\VoucherServicePipelines;

use App\DTO\WalletTransactionDTO;
use App\Exceptions\WalletTransactionFailedException;
use App\Models\Voucher;
use App\Services\PipelineInterface;
use App\Services\WalletService;
use Closure;

class ConsumeVoucher implements PipelineInterface
{

    /**
     * @param $data
     * @param Closure $next
     * @return mixed
     * @throws WalletTransactionFailedException
     */
    public function handle($data, Closure $next): mixed
    {
        $voucher = $data->getVoucher();
        $wallet = $data->getWallet();
        $voucher = $voucher->refresh();
        $amount = $this->getAmountOfVoucher($voucher);
        //locking voucher for update to avoid race condition
        //its necessary to query Voucher like this to lock the row
        Voucher::whereId($voucher->id)->lockForUpdate()->increment('count');

        $walletService = app(WalletService::class);

        $walletTransDTO = new WalletTransactionDTO($wallet, $amount, 'Voucher code');

        $user = $wallet->user;

        $user->vouchers()->attach($voucher->id);

        $walletService->walletTransaction($walletTransDTO);

        return $next($data);
    }


    private function getAmountOfVoucher($voucher)
    {
        if ($voucher->type == 1) {
            //type is charge
            return $voucher->amount;
        }
        //pass type 2 for discount
    }
}
