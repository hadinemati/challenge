<?php

namespace App\Services\VoucherServicePipelines;

use App\Enums\ErrorCodes;
use App\Exceptions\VoucherAlreadyUsedException;
use App\Exceptions\VoucherExpiredException;
use App\Exceptions\VoucherIsNotActiveException;
use App\Exceptions\VoucherNoCapacityException;
use App\Models\UserVoucher;
use App\Services\PipelineInterface;
use Carbon\Carbon;
use Closure;

class CheckVoucher implements PipelineInterface
{

    /**
     * @param $data
     * @param Closure $next
     * @return mixed
     * @throws VoucherAlreadyUsedException
     * @throws VoucherExpiredException
     * @throws VoucherIsNotActiveException
     * @throws VoucherNoCapacityException
     */
    public function handle($data, Closure $next): mixed
    {
        $voucher = $data->getVoucher();
        //check voucher active state
        if (!$voucher->is_active)
            throw new VoucherIsNotActiveException(ErrorCodes::VOUCHER_NOT_ACTIVE);

        if (($voucher->capacity - $voucher->count) <= 0)
            throw new VoucherNoCapacityException(ErrorCodes::VOUCHER_NO_CAPACITY);

        if ($this->isVoucherUsed($data))
            throw new VoucherAlreadyUsedException(ErrorCodes::VOUCHER_ALREADY_USED);

        if (
            Carbon::now()->lt($voucher->start_date) ||
            Carbon::now()->gt($voucher->expiration_date)
        )
            throw new VoucherExpiredException(ErrorCodes::VOUCHER_EXPIRED);


        return $next($data);
    }

    public function isVoucherUsed($data): bool
    {
        $wallet = $data->getWallet();
        $voucher = $data->getVoucher();
        $user = $wallet->user;

        $isUsed = UserVoucher::whereUserId($user->id)
            ->whereVoucherId($voucher->id)
            ->first();

        if ($isUsed)
            return true;
        return false;
    }
}
