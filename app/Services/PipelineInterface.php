<?php

namespace App\Services;

use Closure;

interface PipelineInterface
{
    public function handle($data, Closure $next);
}
