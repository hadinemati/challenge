<?php

namespace App\Services;

use App\DTO\WalletTransactionDTO;
use App\Exceptions\WalletTransactionFailedException;
use App\Models\Transaction;
use App\Models\User;
use DB;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Log;

class WalletService
{
    /**
     * @param WalletTransactionDTO $transactionDTO
     * @return void
     * @throws WalletTransactionFailedException|Exception
     */
    public function walletTransaction(WalletTransactionDTO $transactionDTO): void
    {
        //make transaction in transactions table
        //add the amount to wallet balance, amount can be positive or negative
        //these two phases should be placed in one transaction.
        DB::beginTransaction();
        try {
            //make transaction record in transactions table
            $wallet = $transactionDTO->getWallet();
            $transaction = new Transaction();
            $transaction->amount = $transactionDTO->getTransactionAmount();
            $transaction->wallet_id = $wallet->id;
            $transaction->description = $transactionDTO->getDescription();
            $transaction->save();


            //add lock for update to prevent race conditions
            //add the amount to balance
            //its necessary to query like this to avoid increase of all records and use of lock
            $wallet->whereId($wallet->id)
                ->lockForUpdate()
                ->increment('balance', $transactionDTO->getTransactionAmount());

            DB::commit();
        } catch (Exception  $exception) {
            DB::rollBack();
            Log::error($exception);
            throw  $exception;
        }
    }

    /**
     * creates or the gets the user Wallet
     * @param User $user
     * @return Model
     */
    public function getWallet(User $user): Model
    {
        return $user->wallet()->firstOrCreate();
    }
}
