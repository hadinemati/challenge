<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

/**
 * @property string name
 * @property string mobile
 */
class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    protected $fillable = ['name'];

    public function wallet()
    {
        return $this->hasOne(Wallet::class);
    }

    public function vouchers()
    {
        return $this->belongsToMany(Voucher::class)
            ->using(UserVoucher::class);
    }

    public function transactions()
    {
        return $this->hasManyThrough(
            Transaction::class,
            Wallet::class
        );
    }
}
