<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @property double balance
 * @property int user_id: foreign key which points to user
 */
class Wallet extends Model
{
    use HasFactory;

    protected $attributes = [
        "balance" => 0
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function transactions()
    {
        return $this->hasMany(Transaction::class);
    }
}
