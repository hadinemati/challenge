<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    //todo: handle fillable based on needs
    protected $fillable = [
        'amount',
        'wallet_id',
        'description'
    ];
}
