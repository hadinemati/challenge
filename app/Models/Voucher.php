<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Voucher extends Model
{
    const TYPE_CHARGE = 1;
    const TYPE_DISCOUNT = 2;
    const TYPES = [
        self::TYPE_CHARGE,
        self::TYPE_DISCOUNT
    ];

    use HasFactory;

    //todo: handle fillable based on needs.
    protected $fillable = [
        'code',
        'start_date',
        'expiration_date',
        'capacity',
        'count', //usage count
        'type',
        'amount',
        'is_active'
    ];

    protected $attributes = [
        "count" => 0
    ];
    protected $casts = [
        'start_date' => 'datetime',
        'expiration_date' => 'datetime',
        'amount' => 'double',
        'count' => 'integer',
        'capacity' => 'integer',
        'type' => 'integer'
    ];

    public function users()
    {
        return $this->belongsToMany(User::class)
            ->using(UserVoucher::class);
    }
}
