<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserStoreRequest extends FormRequest
{
    public function rules()
    {
        return [
            'mobile' => [
                'required',
                'string',
                'min:10',
                'max:11',
                'unique:users,mobile'
            ],
            'name' => [
                'required',
                'string',
                'max:24'
            ]
        ];
    }
}
