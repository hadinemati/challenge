<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ConsumeVoucherRequest extends FormRequest
{
    public function rules()
    {
        return [
            'code'=>[
                'string',
                'required',
                'exists:vouchers,code'
            ],
            'mobile'=>[
                'string',
                'min:10',
                'max:12',
                'exists:users,mobile'
            ]
        ];
    }
}
