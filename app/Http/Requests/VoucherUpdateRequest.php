<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class VoucherUpdateRequest extends FormRequest
{
    public function rules()
    {
        return [
            'start_date' => ['nullable', 'date'],
            'expiration_date' => ['nullable', 'date'],
            'capacity' => ['integer'],
            'type' => ['integer'],
            'is_active' => ['boolean'],
            'amount' => ['numeric']
        ];
    }
}
