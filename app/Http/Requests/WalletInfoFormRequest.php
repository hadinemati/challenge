<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class WalletInfoFormRequest extends FormRequest
{
    public function rules()
    {
        return [
            'mobile' => ['required', 'exists:users,mobile'],
        ];
    }
}
