<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class VoucherUsersFormRequest extends FormRequest
{
    public function rules()
    {
        return [
            'code' => [
                'required',
                'string',
                'exists:vouchers,code'
            ]
        ];
    }
}
