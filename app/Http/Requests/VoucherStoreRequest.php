<?php

namespace App\Http\Requests;

use App\Models\Voucher;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class VoucherStoreRequest extends FormRequest
{
    public function rules()
    {
        return [
            'code' => [
                'required',
                'max:10',
                'min:5',
                'unique:vouchers,code'
            ],
            'start_date' => ['date'],
            'expiration_date' => [ 'date'],
            'capacity' => ['required', 'integer'],
            'type' => [
                'required',
                'integer',
                Rule::in(Voucher::TYPES)
            ],
            //todo: add condition for percentage and amount
            'amount' => [
                'required',
                'numeric',
            ],
            'is_active'=>[
                'boolean',
                'required'
            ]
        ];
    }
}
