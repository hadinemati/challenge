<?php

namespace App\Http\Controllers;

use App\Http\Requests\VoucherStoreRequest;
use App\Http\Requests\VoucherUpdateRequest;
use App\Http\Resources\VoucherResource;
use App\Models\Voucher;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Throwable;

class VoucherController extends Controller
{
    /**
     * show list of vouchers
     *
     * @param Request $request
     * @return AnonymousResourceCollection
     */
    public function index(Request $request)
    {
        $perPage = $request->get('perPage') ?? 10;
        $voucherCodes = Voucher::paginate($perPage);

        return VoucherResource::collection($voucherCodes);
    }

    /**
     * show save record
     *
     * @param VoucherStoreRequest $request
     * @return VoucherResource
     */
    public function store(VoucherStoreRequest $request)
    {
        $voucher = new Voucher();
        $voucher->code = $request->get('code');
        $voucher->start_date = $request->get('start_date');
        $voucher->expiration_date = $request->get('expiration_date');
        $voucher->capacity = $request->get('capacity');
        $voucher->type = $request->get('type');
        $voucher->amount = $request->get('amount');
        $voucher->is_active = $request->get('is_active');
        $voucher->save();

        return new VoucherResource($voucher);
    }

    /**
     * show voucher record
     *
     * @param int $id
     * @return VoucherResource
     */
    public function show(int $id)
    {
        $voucher = Voucher::findOrFail($id);
        return new VoucherResource($voucher);
    }

    /**
     * update voucher record
     *
     * @param VoucherUpdateRequest $request
     * @param int $id
     * @return VoucherResource
     * @throws Throwable
     */
    public function update(VoucherUpdateRequest $request, int $id)
    {
        $voucher = Voucher::findOrFail($id);
        $voucher->updateOrFail($request->safe()->all());
        return new VoucherResource($voucher);
    }

    /**
     * delete a record
     *
     * @param int $id
     * @return JsonResponse
     */
    public function destroy(int $id)
    {
        $res = Voucher::findOrFail($id)->delete();
        if ($res) {
            return response()
                ->json(
                    [
                        'message' => __('actionMessages.delete', ['attribute' => 'voucher code'])
                    ]
                );
        }
        return response()
            ->json(
                [
                    'message' => __('actionMessages.delete_fail', ['attribute' => 'voucher code'])
                ]
            );
    }
}
