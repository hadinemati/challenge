<?php

namespace App\Http\Controllers;

use App\Http\Requests\WalletInfoFormRequest;
use App\Http\Resources\TransactionResource;
use App\Http\Resources\WalletResource;
use App\Models\User;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class walletController extends Controller
{
    /**
     * wallet info and transactions
     *
     * @param WalletInfoFormRequest $request
     * @return AnonymousResourceCollection
     */
    public function info(WalletInfoFormRequest $request)
    {
        $mobile = $request->get('mobile');
        $perPage = $request->get('perPage') ?? 10;

        $wallet = User::where('mobile', $mobile)->firstOrFail()->wallet;
        $walletTransactions = $wallet->transactions()->paginate($perPage);


        return TransactionResource::collection($walletTransactions)->additional(
            [
                'walletInfo' => new WalletResource($wallet)
            ]
        );
    }
}
