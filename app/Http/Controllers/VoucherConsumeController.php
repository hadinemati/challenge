<?php

namespace App\Http\Controllers;

use App\DTO\VoucherConsumeDTO;
use App\Http\Requests\ConsumeVoucherRequest;
use App\Models\User;
use App\Models\Voucher;
use App\Services\VoucherService;
use Exception;
use Illuminate\Http\JsonResponse;

class VoucherConsumeController extends Controller
{
    private VoucherService $voucherService;

    public function __construct(VoucherService $voucherService)
    {
        $this->voucherService = $voucherService;
    }

    /**
     * consume a voucher code
     *
     * @param ConsumeVoucherRequest $request
     * @return JsonResponse|void
     * @throws Exception
     */
    public function consumeVoucher(ConsumeVoucherRequest $request)
    {
        $mobile = $request->get('mobile');
        $code = $request->get('code');

        $user = User::where('mobile', $mobile)->firstOrFail();
        $voucher = Voucher::where('code', $code)->firstOrFail();

        $consumeVoucherDto = new VoucherConsumeDTO($voucher, $user->wallet);

        $res = $this->voucherService->consumeVoucher($consumeVoucherDto);
        if ($res)
            return response()->json(['message' => __('actionMessages.voucher_success')]);
    }
}
