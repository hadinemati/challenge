<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserStoreRequest;
use App\Http\Requests\UserUpdateRequest;
use App\Http\Resources\UserResource;
use App\Models\User;
use App\Services\WalletService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Throwable;

class UserController extends Controller
{
    /**
     * show all users
     *
     * @param Request $request
     * @return AnonymousResourceCollection
     */
    public function index(Request $request)
    {
        $perPage = $request->get('perPage') ?? 10;
        $users = User::paginate($perPage);

        return UserResource::collection($users);
    }

    /**
     * create a user
     *
     * @param UserStoreRequest $request
     * @return UserResource
     */
    public function store(UserStoreRequest $request)
    {
        $mobile = $request->get('mobile');
        $name = $request->get('name');
        $user = new User();
        $user->mobile = $mobile;
        $user->name = $name;
        $user->save();

        //make wallet for user
        $walletService = app(WalletService::class);

        $walletService->getWallet($user);

        return new UserResource($user->refresh());
    }

    /**
     * show one user
     *
     * @param int $id
     * @return UserResource
     */
    public function show(int $id)
    {
        $user = User::findOrFail($id);
        return new UserResource($user);
    }

    /**
     * update a user
     *
     * @param UserUpdateRequest $request
     * @param int $id
     * @return UserResource
     * @throws Throwable
     */
    public function update(UserUpdateRequest $request, int $id)
    {
        $user = User::findOrFail($id);
        $user->updateOrFail($request->safe()->all());
        return new UserResource($user);
    }

    /**
     * delete a user
     *
     * @param int $id
     * @return JsonResponse
     */
    public function destroy(int $id)
    {

        $res = User::findOrFail($id)->delete();
        if ($res) {
            return response()
                ->json(
                    [
                        'message' => __('actionMessages.delete', ['attribute' => 'user'])
                    ]
                );
        }
        return response()
            ->json(
                [
                    'message' => __('actionMessages.delete_fail', ['attribute' => 'user'])
                ]
            );
    }
}
