<?php

namespace App\Http\Controllers;

use App\Http\Requests\VoucherUsersFormRequest;
use App\Http\Resources\UserResource;
use App\Models\Voucher;

class ReportController extends Controller
{
    public function voucherUsers(VoucherUsersFormRequest $request)
    {
        $perPage = $request->get('perPage') ?? 10;
        $code = $request->get('code');

        $voucher = Voucher::where('code', $code)->firstOrFail();
        $users = $voucher->users()->paginate($perPage);

        return UserResource::collection($users);
    }
}
