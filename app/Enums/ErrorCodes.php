<?php

namespace App\Enums;

enum ErrorCodes: int
{
    case NOT_FOUND = 404;
    case WALLET_TRANS_FAILED = 1001;
    case VOUCHER_NO_CAPACITY = 1002;
    case VOUCHER_NOT_ACTIVE = 1003;
    case VOUCHER_ALREADY_USED = 1004;
    case VOUCHER_EXPIRED = 1005;

    /**
     * get the error's code
     *
     * @return int
     */
    public function getCode(): int
    {
        return $this->value;
    }

    /**
     * get the errors message
     *
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message($this);
    }

    /**
     * get error message from class.
     *
     * @param ErrorCodes $value
     * @return string
     */
    private function message(self $value): string
    {
        return match ($value) {
            ErrorCodes::NOT_FOUND => __('exceptions.not_found'),
            ErrorCodes::WALLET_TRANS_FAILED => __('exceptions.wallet_trans_failed'),
            ErrorCodes::VOUCHER_NO_CAPACITY => __('exceptions.voucher_no_capacity'),
            ErrorCodes::VOUCHER_NOT_ACTIVE => __('exceptions.voucher_is_not_active'),
            ErrorCodes::VOUCHER_ALREADY_USED => __('exceptions.voucher_already_used'),
            ErrorCodes::VOUCHER_EXPIRED => __('exceptions.voucher_expired'),
        };
    }

    public function getHttpCode(): int
    {
        return $this->httpCode($this);
    }

    private function httpCode(self $value): int
    {
        return match ($value) {
            ErrorCodes::NOT_FOUND => 404,
            ErrorCodes::WALLET_TRANS_FAILED,
            ErrorCodes::VOUCHER_NO_CAPACITY,
            ErrorCodes::VOUCHER_NOT_ACTIVE,
            ErrorCodes::VOUCHER_EXPIRED,
            ErrorCodes::VOUCHER_ALREADY_USED => 422,
            default => 500,
        };
    }
}
