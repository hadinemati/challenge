<?php

namespace App\Providers;

use App\Services\VoucherService;
use App\Services\WalletService;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        $this->app->singleton(WalletService::class,function (){
            return new WalletService();
        });
        $this->app->singleton(VoucherService::class,function (){
            return new VoucherService();
        });
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        //
    }
}
