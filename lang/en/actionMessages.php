<?php

return [
    'delete' => 'The :attribute deleted.',
    'delete_fail' => 'something went wrong',
    'voucher_success' => 'using voucher was successful',
];
