<?php
/**
 * messages which will be used for output for exceptions
 */
return [
    'not_found' => 'record not found',
    'wallet_trans_failed' => 'adding to wallet failed',
    'voucher_no_capacity' => 'voucher capacity ended',
    'voucher_is_not_active' => 'voucher is not active',
    'voucher_expired' => 'voucher expired',
    'voucher_already_used' => 'voucher is used',
];
