<?php

use App\Http\Controllers\ReportController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\VoucherConsumeController;
use App\Http\Controllers\VoucherController;
use App\Http\Controllers\walletController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware(['throttle:api'])
    ->resource('users', UserController::class);

Route::middleware(['throttle:api'])
    ->resource('vouchers', VoucherController::class);

//TODO: add RateLimit
Route::middleware(['throttle:report'])
    ->prefix('reports')->group(function () {
    Route::get('/voucherUsers', [ReportController::class, 'voucherUsers']);
});

Route::middleware(['throttle:api'])
    ->prefix('wallet')->group(function () {
    Route::get('/info', [walletController::class, 'info']);
});


Route::middleware(['throttle:voucher'])
    ->post(
        '/consumeVoucher',
        [VoucherConsumeController::class, 'consumeVoucher']
    );
